import random
import string

from numpy.core.shape_base import block
import lib
import numpy as np
import copy


class Chromosome:
    
    def __init__(self, target, genetic_code, fitness):
        self.genetic_code = genetic_code
        self.fitness = fitness
        self.target = target
        self.recombination_aid = np.zeros(len(self.genetic_code) + 1)
    def __str__(self):
        return "{} = {}".format(''.join(lib.sc_superstring(self.target, self.genetic_code)), self.fitness)

class Block():
    def __init__(self, code, fitness):
        self.code = code
        self.fitness = fitness

    def __str__(self):
        return "{} = {}".format(self.code, self.fitness)

class Puzzle:
    
    def __init__(self, possible_gene_values, target, generation_size=100, max_iterations=1000, mutation_rate=0.1):
        self.target = target                                    
        self.possible_gene_values = possible_gene_values         
        
     
        self.generation_size = generation_size        
        self.chromosome_size = len(target)     
        self.reproduction_size = 50
        self.max_iterations = max_iterations
        self.mutation_rate = mutation_rate
        self.block_generation_size = 200
        self.reproduction_size_blocks = 100 

    def calculate_fitness_block(self, population, block_code):
        sum = 0.0
        num = 0
        for ch in population: 
            if self.contains(ch.genetic_code, block_code):
                sum += ch.fitness
                num += 1

        if num == 0:
            return 0.0

        return sum/num  

    def contains(self, genetic_code, block_code):

        try:
            first_index = genetic_code.index(block_code[0])    
        except ValueError :
            return False
        
        if first_index + len(block_code) >= len(genetic_code) :
            return False

        j = 0
        for i in range(first_index, first_index + len(block_code)):
            if genetic_code[i] != block_code[j]:
                return False 
            j += 1
                
        return True

    def calculate_fitness(self, genetic_code):
       
        strsp = lib.sc_superstring(self.target, genetic_code)
        
        ms=0
        for s in self.target:
          if strsp.find(s) == -1:
            ms += len(s)

        return 1.0/(len(strsp) + ms)**2    


    def initial_population_block(self, population):
        init_blocks = []
        i = 0
        while i < self.block_generation_size:
            r = random.sample(self.possible_gene_values, k=2)
            if init_blocks != [] and init_blocks.count(r) > 0:
                continue
            for ch in population:
                if self.contains(ch.genetic_code, r) :
                    block = Block(r, self.calculate_fitness_block(population, r))
                    self.update_recombination_vector(population, block)
                    init_blocks.append(block)
                    break
            i += 1
        
        return init_blocks

    def initial_population(self):
        init_population = []
        
        for _ in range(self.generation_size):
         
            genetic_code = []
            
            genetic_code = random.sample(population= self.possible_gene_values, k=len(self.possible_gene_values))
                     
            fitness = self.calculate_fitness(genetic_code)
            new_chromosome = Chromosome(self.target, genetic_code, fitness)

            init_population.append(new_chromosome)
            
        return init_population

    def update_recombination_vector(self, population, block):
        for ch in population:
            if self.contains(ch.genetic_code, block.code):
                first_index = ch.genetic_code.index(block.code[0])+1
                
                for i in range(first_index, first_index + len(block.code)):
                    if ch.recombination_aid[i] < block.fitness:
                        ch.recombination_aid[i] = block.fitness
                
                ch.recombination_aid[0] = ch.recombination_aid[1]
                ch.recombination_aid[len(ch.recombination_aid)-1] = ch.recombination_aid[len(ch.recombination_aid)-2]

    def selection(self, chromosomes):
        
        selected = []
        
        for _ in range(self.reproduction_size):
            selected.append(self.roulette_selection(chromosomes))
    
        return selected
    def roulette_selection_blocks(self, blocks):
        total_fitness = sum([block.fitness for block in blocks])
        #print(total_fitness)
        selected_value = random.random()*total_fitness
        
        current_sum = 0
        for i in range(self.block_generation_size):
            current_sum += blocks[i].fitness

            if current_sum >= selected_value:
                return blocks[i]
       

    def roulette_selection(self, chromosomes):
    
        total_fitness = sum([chromosome.fitness for chromosome in chromosomes])
        
        selected_value = random.random()*total_fitness
        
        current_sum = 0
        for i in range(self.block_generation_size):
            current_sum += chromosomes[i].fitness

            if current_sum >= selected_value:
                return chromosomes[i]
     
    def mutate(self, genetic_code):
        random_value = random.random()
        
        if random_value < self.mutation_rate:
            
            random_index = random.randrange(self.chromosome_size)
            
            while True:
                new_value = random.choice(self.possible_gene_values)
                
                if genetic_code[random_index] != new_value:
                    break
                    
            genetic_code[random_index] = new_value
            
        return genetic_code

    def create_generation(self, chromosomes):
        
        generation = []
        generation_size = 0
        
        while generation_size < self.generation_size:
    
            r = random.random()

            [parent1, parent2] = random.sample(chromosomes, 2)
                
            if r < 0.3:
                child1_code, child2_code = self.crossover(parent1, parent2)
            else :
               child1_code, child2_code = self.crossover_puzzle(parent1, parent2)
            

            child1_code = self.mutate(child1_code)
            child2_code = self.mutate(child2_code)
            
            child1 = Chromosome(self.target, child1_code, self.calculate_fitness(child1_code))
            child2 = Chromosome(self.target, child2_code, self.calculate_fitness(child2_code))
            
            generation.append(child1)
            generation.append(child2)
            
            generation_size += 2

        return generation

    def create_generation_blocks(self,population, blocks):

        for _ in range(self.reproduction_size_blocks):
            block = self.roulette_selection_blocks(blocks)
            i = blocks.index(block)

            r_expansion = random.random()
            r_exploration = random.random()

            if (r_expansion < 0.8):
                block = self.expansion(population, block)
            if (r_exploration < 0.1):
                block = self.exploration(population)

            blocks[i] = block 


        return blocks
        
    def exploration(self, population):
        while True:
            r = random.sample(self.possible_gene_values, k=2)
            for ch in population:
                if self.contains(ch.genetic_code, r) :
                    block = Block(r, self.calculate_fitness_block(population, r))
                    self.update_recombination_vector(population, block)
                    return block

    def expansion(self, population, block):
        
        r = random.sample(self.possible_gene_values, k=1)[0]
        code_copy = copy.deepcopy(block.code);
        code_copy.append(r)
        code_copy_r = [r].append(code_copy);
        for code in [code_copy, code_copy_r] :
            
            possible_values = []        
            for ch in population:
                #Ukoliko sadrzi novi kod
                if self.contains(ch.genetic_code, code) :
                    block.fitness = self.calculate_fitness_block(population, block.code)
                    self.update_recombination_vector(population, block)
                    return block
                #Ukoliko ne sadrzi prosireni blok ali je sadrzala stari
                elif self.contains(ch.genetic_code, block.code):
                    first_index = ch.genetic_code.index(code[0])    
                    # i ukoliko se ne nalazi na samom kraju gena
                    index = first_index + len(code)-1 
                    if index < len(ch.genetic_code):
                        possible_values.append(ch.genetic_code[index])

                # Ukoliko ne postoji nijedna vrednost koja moze da se doda vracamo stari blok
                if possible_values == []:
                    continue
                else:
                    block.code.append(random.sample(possible_values, k=1)[0])
                    block.fitness = self.calculate_fitness_block(population, block.code)
                    self.update_recombination_vector(population, block)
                    return block
        return block
    def crossover_puzzle(self, parent1, parent2):

        min_value_p1 = np.min(parent1.recombination_aid)
        index_values_p1 = np.hstack(np.argwhere(parent1.recombination_aid == min_value_p1))
        
        if len(index_values_p1) == 1:
            break_point_p1 = index_values_p1[0]
        else:
            break_point_p1 = index_values_p1[random.randrange(len(index_values_p1))]

        min_value_p2 = np.min(parent2.recombination_aid)
        index_values_p2 = np.hstack(np.argwhere(parent2.recombination_aid == min_value_p2))

        if len(index_values_p2) == 1:
            break_point_p2 = index_values_p2[0]
        else:
            break_point_p2 = index_values_p2[random.randrange(len(index_values_p2))]
        
        if break_point_p1 < break_point_p2:
            child1 = parent1.genetic_code[:break_point_p1] + parent2.genetic_code[break_point_p1:break_point_p2] + parent1.genetic_code[break_point_p2:]
            child2 = parent2.genetic_code[:break_point_p1] + parent1.genetic_code[break_point_p1:break_point_p2] + parent2.genetic_code[break_point_p2:] 
        else :
            child1 = parent1.genetic_code[:break_point_p2] + parent2.genetic_code[break_point_p2:break_point_p1] + parent1.genetic_code[break_point_p1:]
            child2 = parent2.genetic_code[:break_point_p2] + parent1.genetic_code[break_point_p2:break_point_p1] + parent2.genetic_code[break_point_p1:]

        return child1, child2
        
    def crossover(self, parent1, parent2):
        
        break_point_p1, break_point_p2 = random.sample([*range(len(parent1.genetic_code))], 2)

        if break_point_p1 < break_point_p2:
            child1 = parent1.genetic_code[:break_point_p1] + parent2.genetic_code[break_point_p1:break_point_p2] + parent1.genetic_code[break_point_p2:]
            child2 = parent2.genetic_code[:break_point_p1] + parent1.genetic_code[break_point_p1:break_point_p2] + parent2.genetic_code[break_point_p2:] 
        else :
            child1 = parent1.genetic_code[:break_point_p2] + parent2.genetic_code[break_point_p2:break_point_p1] + parent1.genetic_code[break_point_p1:]
            child2 = parent2.genetic_code[:break_point_p2] + parent1.genetic_code[break_point_p2:break_point_p1] + parent2.genetic_code[break_point_p1:]
            
        return child1, child2

    def test(self):
        
        population = self.initial_population()
        
        blocks = self.initial_population_block(population)
     
        global_best_chromosome = max(population, key=lambda x: x.fitness)
        
        for i in range(self.max_iterations):
            
            selected = self.selection(population)

            population = self.create_generation(selected)
            for j in range(self.block_generation_size):
                fitness = self.calculate_fitness_block(population, blocks[j].code)
                if fitness == 0:
                    blocks[j] = self.exploration(population)
            

           

            blocks = self.create_generation_blocks(population, blocks)

          
            best_chromosome = max(population, key=lambda x: x.fitness)

            if global_best_chromosome.fitness < best_chromosome.fitness:
              global_best_chromosome = best_chromosome

            global_best = lib.sc_superstring(self.target, global_best_chromosome.genetic_code)
            for s in self.target:
                if global_best.find(s) == -1:
                    global_best += s
    
            

        global_best = lib.sc_superstring(self.target, global_best_chromosome.genetic_code)
        for s in self.target:
            if global_best.find(s) == -1:
                global_best += s

        return global_best

