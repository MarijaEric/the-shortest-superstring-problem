import random
import string
import lib

class Chromosome:
    
    def __init__(self, target, genetic_code, fitness):
        self.genetic_code = genetic_code
        self.fitness = fitness
        self.target = target
    
    def __str__(self):
        return "{}: {} = {}".format(self.genetic_code, ''.join(lib.sc_superstring(self.target, self.genetic_code)), self.fitness)

class GeneticAlgorithm:
    
    def __init__(self, possible_gene_values, target):
        self.target = target                                    
        self.possible_gene_values = possible_gene_values         
        
    
        self.generation_size = 500          
        self.chromosome_size = len(target)     
        self.reproduction_size = 100           
        self.max_iterations = 1000         
        self.mutation_rate = 0.1           
        

    def calculate_fitness(self, genetic_code):
       
        strsp = lib.sc_superstring(self.target, genetic_code)
        
        ms=0
        for s in self.target:
          if strsp.find(s) == -1:
            ms += len(s)

        return 1.0/(len(strsp) + ms)**2    


    def initial_population(self):
        init_population = []
        
        for _ in range(self.generation_size):
         
            genetic_code = []
            
            genetic_code = random.sample(population= self.possible_gene_values, k=len(self.possible_gene_values))
                     
            fitness = self.calculate_fitness(genetic_code)
            new_chromosome = Chromosome(self.target, genetic_code, fitness)

            init_population.append(new_chromosome)
            
        return init_population
    
    def selection(self, chromosomes):
    
        selected = []
        
        for _ in range(self.reproduction_size):
            selected.append(self.roulette_selection(chromosomes))
    
        return selected
    
    def roulette_selection(self, chromosomes):
    
        total_fitness = sum([chromosome.fitness for chromosome in chromosomes])
        
        selected_value = random.random()*total_fitness
        
        current_sum = 0
        for i in range(self.generation_size):
            current_sum += chromosomes[i].fitness

            if current_sum > selected_value:
                return chromosomes[i]
     
    def mutate(self, genetic_code):
        random_value = random.random()
        
        if random_value < self.mutation_rate:
            
            random_index = random.randrange(self.chromosome_size)
            
            while True:
                new_value = random.choice(self.possible_gene_values)
                
                if genetic_code[random_index] != new_value:
                    break
                    
            genetic_code[random_index] = new_value
            
        return genetic_code
        
    def create_generation(self, chromosomes):
        generation = []
        generation_size = 0
        
        while generation_size < self.generation_size:
    
            [parent1, parent2] = random.sample(chromosomes, 2)
            
            child1_code, child2_code = self.crossover(parent1, parent2)
            
            child1_code = self.mutate(child1_code)
            child2_code = self.mutate(child2_code)
            
            child1 = Chromosome(self.target, child1_code, self.calculate_fitness(child1_code))
            child2 = Chromosome(self.target, child2_code, self.calculate_fitness(child2_code))
            
            generation.append(child1)
            generation.append(child2)
            
            generation_size += 2
            
        return generation
            
    def crossover(self, parent1, parent2):
        
        break_point = random.randrange(1, self.chromosome_size)
        
        child1 = parent1.genetic_code[:break_point] + parent2.genetic_code[break_point:]
        child2 = parent2.genetic_code[:break_point] + parent1.genetic_code[break_point:]
        
        return (child1, child2)
        
    def optimize(self):
    
        population = self.initial_population()
        
        global_best_chromosome = max(population, key=lambda x: x.fitness)
        for _ in range(0, self.max_iterations):
       
            selected = self.selection(population)
       
            population = self.create_generation(selected)
            
                
            best_chromosome = max(population, key=lambda x: x.fitness)

            if global_best_chromosome.fitness < best_chromosome.fitness:
              global_best_chromosome = best_chromosome

        global_best = lib.sc_superstring(self.target, global_best_chromosome.genetic_code)
        for s in self.target:
            if global_best.find(s) == -1:
                global_best += s

        return global_best
            


