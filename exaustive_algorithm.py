import lib
import string
import random
import numpy as np

def next_permutation(a):
      
    """Generate the lexicographically next permutation inplace.

    Return false if there is no next permutation.

    """
    # Find the largest index i such that a[i] < a[i + 1]. If no such
    # index exists, the permutation is the last permutation
    for i in reversed(range(len(a) - 1)):
        if a[i] < a[i + 1]:
            break 
    else:  
        return False  # no next permutation

    # Find the largest index j greater than i such that a[i] < a[j]
    j = next(j for j in reversed(range(i + 1, len(a))) if a[i] < a[j])

    # Swap the value of a[i] with that of a[j]
    a[i], a[j] = a[j], a[i]

    # Reverse sequence from a[i + 1] up to and including the final element a[n]
    a[i + 1:] = reversed(a[i + 1:])
    return True



def exaustive_algorithm(S):
    """Implementation of exaustive algorithm for The Shortest Common Superstring Problem.
    Return shortest common superstring.
    """

    # Initialization of shortest superstring
    opt_s_size = sum([len(s) for s in S])
    opt_s = ''.join(S)

    # Initialization of first permutation
    p = [*range(len(S))]

    # Founding superstring for every permutation of strings in S, and returning shortest one
    
    while True: 
      
      #Finding of shortest superstring of S for permutation p
      s = lib.sc_superstring(S, p)

      # Check if we got shorter superstring
      if len(s) < opt_s_size:
        opt_s = s
        opt_s_size = len(s)

      # No next permutations
      if next_permutation(p) == False:
        break


    return opt_s

