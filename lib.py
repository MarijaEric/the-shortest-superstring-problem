import numpy as np
import string
import random


def overlap(str_a, str_b):
    if str_b == "" or str_a == "":
      return 0
    
    index_last = str_a.rfind(str_b[0])

    if str_b.find(str_a[index_last:]) == 0:
      return len(str_a) - index_last
    else :
      return 0


def sc_superstring(S, permutation):
    s :string = S[permutation[0]]

    for i in range(1, len(permutation)):
        if s.find(S[permutation[i]]) == -1 :
          
          s1 = S[permutation[i]]

          j = overlap(s, s1)
          s = s + s1[j:]
          
    return s
