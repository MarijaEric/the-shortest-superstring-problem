import copy
import lib


def max_overlap(S):
    """
        Implementation of max_overlap function for S.
        Return indices of strings in S that have maximum overlap.
    """
    # Initialization of indices and overlap value.
    max_i = -1
    max_j = -1
    max_overlap_len = 0

    # Iterating trough all combinations of two strings in S and evaluating overlap value.
    for i in range(len(S)):
        for j in range(len(S)):

            if i != j :
                overlap_len = lib.overlap(S[i], S[j])
                #Checking if larger overlap was found.
                if overlap_len > max_overlap_len:
                    max_overlap_len = overlap_len
                    max_i = i
                    max_j = j


    return max_i, max_j, max_overlap_len

def greedy_algorithm(S1):
    
    """
        Implementation of greedy algorithm for The Shortest Common Superstring Problem.
        
    """
    S = copy.deepcopy(S1)

    # Calling od max_overlap function while there is more then one string left.
    
    while len(S) > 1:
        
        max_i, max_j, max_overlap_len = max_overlap(S)
        
        # if there is overlap between any of strings - concanate all of them.
        
        if max_i == -1 and max_j == -1:
            return ''.join(S)

        # Adding new merged string in S    
        S.append(S[max_i] + S[max_j][max_overlap_len:])
        
        # Removing old strings from S.
        if max_i > max_j:
            S.remove(S[max_i])
            S.remove(S[max_j])
        else:
            S.remove(S[max_j])
            S.remove(S[max_i])
        
    return ''.join(S)
